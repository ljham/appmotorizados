package ec.com.veris.appmotorizados.beans;

/**
 * Created by luis on 04/11/2016.
 */
public class Profesionales {
    private Integer codigoProfesional;
    private String nombreProfesional;
    private Short codigoDispositivo;
    private String nombreDispositivo;
    private String imei;
    private String macAddressImpresoras;

    public Integer getCodigoProfesional() {
        return codigoProfesional;
    }

    public void setCodigoProfesional(Integer codigoProfesional) {
        this.codigoProfesional = codigoProfesional;
    }

    public String getNombreProfesional() {
        return nombreProfesional;
    }

    public void setNombreProfesional(String nombreProfesional) {
        this.nombreProfesional = nombreProfesional;
    }

    public Short getCodigoDispositivo() {
        return codigoDispositivo;
    }

    public void setCodigoDispositivo(Short codigoDispositivo) {
        this.codigoDispositivo = codigoDispositivo;
    }

    public String getNombreDispositivo() {
        return nombreDispositivo;
    }

    public void setNombreDispositivo(String nombreDispositivo) {
        this.nombreDispositivo = nombreDispositivo;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMacAddressImpresoras() {
        return macAddressImpresoras;
    }

    public void setMacAddressImpresoras(String macAddressImpresoras) {
        this.macAddressImpresoras = macAddressImpresoras;
    }
}
