package ec.com.veris.appmotorizados.beans;

import java.util.List;

/**
 * Created by luis on 15/09/2016.
 */
public class Resultado<T> {

    private String error;
    private List<T> lista;
    private Class<T> objeto;
    private String resultado;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<T> getLista() {
        return lista;
    }

    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    public Class<T> getObjeto() {
        return objeto;
    }

    public void setObjeto(Class<T> objeto) {
        this.objeto = objeto;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

}
