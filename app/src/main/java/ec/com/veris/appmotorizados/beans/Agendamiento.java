package ec.com.veris.appmotorizados.beans;

import java.util.List;

/**
 * Created by luis.jama on 13/09/2016.
 */
public class Agendamiento {

    private Integer secuenciaAgendamiento;
    private Integer codigoProfesional;
    private String fechaAtencion;
    private String horaAtencion;
    private Integer numeroSolicitud;
    private String usuarioAgendamiento;
    private String fechaAgendamiento;

    public Integer getSecuenciaAgendamiento() {
        return secuenciaAgendamiento;
    }

    public void setSecuenciaAgendamiento(Integer secuenciaAgendamiento) {
        this.secuenciaAgendamiento = secuenciaAgendamiento;
    }

    public Integer getCodigoProfesional() {
        return codigoProfesional;
    }

    public void setCodigoProfesional(Integer codigoProfesional) {
        this.codigoProfesional = codigoProfesional;
    }

    public String getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(String fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public String getHoraAtencion() {
        return horaAtencion;
    }

    public void setHoraAtencion(String horaAtencion) {
        this.horaAtencion = horaAtencion;
    }

    public Integer getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(Integer numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getUsuarioAgendamiento() {
        return usuarioAgendamiento;
    }

    public void setUsuarioAgendamiento(String usuarioAgendamiento) {
        this.usuarioAgendamiento = usuarioAgendamiento;
    }

    public String getFechaAgendamiento() {
        return fechaAgendamiento;
    }

    public void setFechaAgendamiento(String fechaAgendamiento) {
        this.fechaAgendamiento = fechaAgendamiento;
    }
}
