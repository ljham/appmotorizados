package ec.com.veris.appmotorizados;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ec.com.veris.appmotorizados.beans.Agendamiento;
import ec.com.veris.appmotorizados.beans.Resultado;
import ec.com.veris.appmotorizados.beans.Solicitudes;
import ec.com.veris.appmotorizados.data.dao.SQLite;
import ec.com.veris.appmotorizados.util.StackTraceUtil;
import ec.com.veris.appmotorizados.ws.VolleyCallback;
import ec.com.veris.appmotorizados.ws.WebService;
import ec.com.veris.appmotorizados.util.Mapper;

public class MainActivity extends AppCompatActivity {
    private String URL_BASE = "http://10.20.10.6:8082/MiWsRest/servicio/agendamiento/";
    private static final String URL_JSON = "obtenerAgenda";
    public static final String TAG = MainActivity.class.getSimpleName();
    private WebService ws = new WebService(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sincronizar(View v) {
        Map<String, String> params = new HashMap<>();
        params.put("arg0", "352405069762305");
        params.put("arg1", "30/08/2016");

        ws.consumirWsGET(URL_BASE + URL_JSON, params, new VolleyCallback() {
            @Override
            public void onSuccessResponse(String resultado) {
                try {
                    if (resultado != null) {
                        Resultado rs = new Mapper<Resultado>().jsonToObject(resultado, new TypeToken<Resultado<Solicitudes>>(){});

                        if(rs!= null){
                            if(rs.getError() == null){
                                List<Solicitudes> ls = rs.getLista();
                                if(ls!= null && !ls.isEmpty()){
                                    for (Solicitudes so: ls) {
                                        Log.d(TAG, "RESULTADO: " + so.getNombreSolicitante());
                                    }
                                    SQLite sql = new SQLite(getApplicationContext());
                                    sql.getConexion();
                                    sql.close();
                                    copyDatabase();
                                }
                            }else{
                                Log.d(TAG, rs.getError());
                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(String error) {
                Log.d(TAG, "" + error);
            }
        });
    }

    public void copyDatabase(){
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/servicioDomicilio.db";
                String backupDBPath = "backupname.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();

                    Log.d(TAG, "" + "Base datos copiada exitosamente!!!");
                }
            }
        } catch (Exception e) {
            String strError = StackTraceUtil.getCustomStackTrace(e);
            Log.d(TAG, "" + strError);
        }
    }
}