package ec.com.veris.appmotorizados.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class StackTraceUtil {
	
	public static String getStackTrace(Throwable aThrowable) {
		StringWriter result = new StringWriter();
		PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	/**
	 * Defines a custom format for the stack trace as String.
	 */
	public static String getCustomStackTrace(Throwable aThrowable) {
		//add the class name and any message passed to constructor
		StringBuilder result = new StringBuilder("ERROR: " );
		result.append(aThrowable.toString());
		String NEW_LINE = System.getProperty("line.separator");
		result.append(NEW_LINE);

		//add each element of the stack trace
		int i = 0;
		for (StackTraceElement element : aThrowable.getStackTrace()){
			if(i < 4){
				result.append(element);
				result.append(NEW_LINE);
			}
			i++;
		}
		return result.toString();
	}

	/** Demonstrate output.  */
	public static void main (String... aArguments){
		Throwable throwable = new IllegalArgumentException("Blah");
		System.out.println(getStackTrace(throwable));
		System.out.println(getCustomStackTrace(throwable));
	}

}
