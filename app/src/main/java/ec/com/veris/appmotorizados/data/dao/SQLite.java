package ec.com.veris.appmotorizados.data.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import ec.com.veris.appmotorizados.beans.Solicitudes;

/**
 * Created by luis on 27/11/2016.
 * Clase para administrar la conexión de la base de datos y su estructuración
 */
public class SQLite extends SQLiteOpenHelper {

    private final Context context;
    private static final String DB = "servicioDomicilio.db";
    private static final int VERSION = 1;

    public SQLite(Context context){
        super(context, DB, null, VERSION);
        this.context = context;
    }

    interface Tablas{
        String PARAMETROS = "PARAMETROS";
        String SOLICITUDES = "SOLICITUDES";
        String AGENDAMIENTO = "AGENDAMIENTO";
        String ORDENES_X_SOLICITUD = "ORDENES_X_SOLICITUD";
        String DETALLE_ORDEN_SERVICIO = "DETALLE_ORDEN_SERVICIO";
        String FORMAS_PAGO_X_SOLICITUD = "FORMAS_PAGO_X_SOLICITUD";
    }

    interface Referencias{
        String ID_SOLICITUD = String.format("REFERENCES %s(%s)",Tablas.SOLICITUDES, Esquema.Solicitudes.NUMERO_SOLICITUD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableSolicitudes());
        db.execSQL(createTableAgendamiento());
        db.execSQL(createTableOrdenesXSolicitud());
        db.execSQL(createTableDetalleOrdenServicio());
        db.execSQL(createTableFormasPagoXSolicitud());
    }

    public SQLiteDatabase getConexion(){
        return this.getReadableDatabase();
    }

    public static void closeConexion(SQLiteDatabase db){
        if(db!= null && !db.isOpen()){
            db.close();
        }
    }

    public String createTableParametros(){
        return null;
    }

    public String createTableSolicitudes(){
        String sql = "CREATE TABLE %s ( " +
                " %s INTEGER PRIMARY KEY," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT " +
                " )";
        sql = String.format(sql,Tablas.SOLICITUDES,
                Esquema.Solicitudes.NUMERO_SOLICITUD,
                Esquema.Solicitudes.ESTADO,
                Esquema.Solicitudes.ORIGEN,
                Esquema.Solicitudes.NOMBRE_SOLICITANTE,
                Esquema.Solicitudes.NUMERO_TELEFONO,
                Esquema.Solicitudes.TIPO_DIRECCION,
                Esquema.Solicitudes.DIRECCION_PRINCIPAL,
                Esquema.Solicitudes.DIRECCION_REFERENCIAL,
                Esquema.Solicitudes.OBSERVACIONES_CALL,
                Esquema.Solicitudes.OBSERVACIONES_FLEBO,
                Esquema.Solicitudes.LATITUD,
                Esquema.Solicitudes.LONGITUD,
                Esquema.Solicitudes.CODIGO_TIPO_IDENT_FACTURA,
                Esquema.Solicitudes.NOMBRE_TIPO_IDENT_FACTURA,
                Esquema.Solicitudes.NOMBRE_FACTURA,
                Esquema.Solicitudes.CORREO_ELECTRONICO_FACTURA,
                Esquema.Solicitudes.IMEI_DISPOSITIVO,
                Esquema.Solicitudes.ES_ACTIVO
            );
        return sql;
    }

    public String createTableAgendamiento(){
        String sql = "CREATE TABLE %s (" +
                " %s INTEGER PRIMARY KEY, " +
                " %s INTEGER %s, " +
                " %s INTEGER," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT" +
                " )";
        sql = String.format(sql,Tablas.AGENDAMIENTO,
                Esquema.Agendamiento.SECUENCIA_AGENDAMIENTO,
                Esquema.Agendamiento.NUMERO_SOLICITUD, Referencias.ID_SOLICITUD,
                Esquema.Agendamiento.CODIGO_PROFESIONAL,
                Esquema.Agendamiento.FECHA_ATENCION,
                Esquema.Agendamiento.HORA_ATENCION,
                Esquema.Agendamiento.USUARIO_AGENDAMIENTO,
                Esquema.Agendamiento.FECHA_AGENDAMIENTO,
                Esquema.Agendamiento.ES_ACTIVO
                );
        return sql;
    }

    public String createTableOrdenesXSolicitud(){
        String sql = "CREATE TABLE %s (" +
                " %s INTEGER %s, " +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s INTEGER," +
                " %s TEXT," +
                " PRIMARY KEY (%s, %s)" +
                " )";
        sql = String.format(sql,Tablas.ORDENES_X_SOLICITUD,
                Esquema.OrdenesXSolicitud.NUMERO_SOLICITUD, Referencias.ID_SOLICITUD,
                Esquema.OrdenesXSolicitud.SECUENCIAL,
                Esquema.OrdenesXSolicitud.CODIGO_EMPRESA,
                Esquema.OrdenesXSolicitud.NUMERO_ORDEN,
                Esquema.OrdenesXSolicitud.TIPO_FACTURACION,
                Esquema.OrdenesXSolicitud.CODIGO_TIPO_IDENTIFICACION,
                Esquema.OrdenesXSolicitud.NOMBRE_TIPO_IDENTIFICACION,
                Esquema.OrdenesXSolicitud.NUMERO_IDENTIFICACION,
                Esquema.OrdenesXSolicitud.NOMBRE_PACIENTE,
                Esquema.OrdenesXSolicitud.FECHA_NACIMIENTO,
                Esquema.OrdenesXSolicitud.EDAD,
                Esquema.OrdenesXSolicitud.GENERO,
                Esquema.OrdenesXSolicitud.ES_DISCAPACITADO,
                Esquema.OrdenesXSolicitud.MAIL,
                Esquema.OrdenesXSolicitud.TELEFONO_FIJO,
                Esquema.OrdenesXSolicitud.TELEFONO_MOVIL,
                Esquema.OrdenesXSolicitud.NOMBRE_CONVENIO,
                Esquema.OrdenesXSolicitud.NOMBRE_PAQUETE,
                Esquema.OrdenesXSolicitud.SECUENCIA_COMPROBANTE,
                Esquema.OrdenesXSolicitud.ES_ACTIVO,
                Esquema.OrdenesXSolicitud.NUMERO_SOLICITUD, Esquema.OrdenesXSolicitud.SECUENCIAL
        );
        return sql;
    }

    public String createTableDetalleOrdenServicio(){
        String sql = "CREATE TABLE %s (" +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s INTEGER," +
                " %s REAL," +
                " %s REAL," +
                " %s REAL," +
                " %s REAL," +
                " %s REAL," +
                " %s TEXT," +
                " %s TEXT," +
                " PRIMARY KEY (%s, %s, %s)" +
                ")";
        sql = String.format(sql, Tablas.DETALLE_ORDEN_SERVICIO,
                Esquema.DetalleOrdenServicio.CODIGO_EMPRESA,
                Esquema.DetalleOrdenServicio.NUMERO_ORDEN,
                Esquema.DetalleOrdenServicio.LINEA_DETALLE,
                Esquema.DetalleOrdenServicio.CODIGO_PRESTACION,
                Esquema.DetalleOrdenServicio.NOMBRE_PRESTACION,
                Esquema.DetalleOrdenServicio.CANTIDAD,
                Esquema.DetalleOrdenServicio.SUBTOTAL_VENTA,
                Esquema.DetalleOrdenServicio.SUBTOTAL_CLIENTE,
                Esquema.DetalleOrdenServicio.SUBTOTAL_COPAGO,
                Esquema.DetalleOrdenServicio.VALOR_IVA,
                Esquema.DetalleOrdenServicio.VALOR_TOTAL,
                Esquema.DetalleOrdenServicio.CODIGO_ESTADO,
                Esquema.DetalleOrdenServicio.ES_ACTIVO,
                Esquema.DetalleOrdenServicio.CODIGO_EMPRESA, Esquema.DetalleOrdenServicio.NUMERO_ORDEN, Esquema.DetalleOrdenServicio.LINEA_DETALLE
                );
        return sql;
    }

    public String createTableFormasPagoXSolicitud(){
        String sql = "CREATE TABLE %s (" +
                " %s INTEGER %s," +
                " %s INTEGER," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s REAL," +
                " %s INTEGER," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " %s TEXT," +
                " PRIMARY KEY (%s, %s)" +
                ")";
        sql = String.format(sql, Tablas.FORMAS_PAGO_X_SOLICITUD,
                Esquema.FormasPagoXSolicitud.NUMERO_SOLICITUD, Referencias.ID_SOLICITUD,
                Esquema.FormasPagoXSolicitud.SECUENCIA,
                Esquema.FormasPagoXSolicitud.CODIGO_FORMA_PAGO,
                Esquema.FormasPagoXSolicitud.NOMBRE_FORMA_PAGO,
                Esquema.FormasPagoXSolicitud.VALOR,
                Esquema.FormasPagoXSolicitud.CODIGO_INSTITUCION,
                Esquema.FormasPagoXSolicitud.NOMBRE_INSTITUCION,
                Esquema.FormasPagoXSolicitud.NUMERO_CHEQUE,
                Esquema.FormasPagoXSolicitud.NUMERO_CUENTA,
                Esquema.FormasPagoXSolicitud.NOMBRE_CUENTA,
                Esquema.FormasPagoXSolicitud.ES_ACTIVO,
                Esquema.FormasPagoXSolicitud.NUMERO_SOLICITUD, Esquema.FormasPagoXSolicitud.SECUENCIA
                );
        return sql;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }




}
