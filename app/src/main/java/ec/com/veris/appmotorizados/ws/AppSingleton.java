package ec.com.veris.appmotorizados.ws;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by luis.jama on 13/09/2016.
 */
public final class AppSingleton extends Application {

    public static final String TAG = AppSingleton.class.getSimpleName();
    private static AppSingleton appSingleton;
    private RequestQueue requestQueue;
    private static Context context;


    private AppSingleton(Context context){
        this.context = context;
        this.requestQueue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if(requestQueue == null){
            // Crear nueva cola de peticiones
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    public static synchronized AppSingleton getInstance(Context context){
        if(appSingleton == null){
            appSingleton = new AppSingleton(context);
        }
        return  appSingleton;
    }

    public <T> void addToRequestQueue(Request<T> request){
        request.setTag(TAG);
        requestQueue.add(request);
    }

    public <T> void addToRequestQueue(Request<T> request, String strTag){
        request.setTag(TextUtils.isEmpty(strTag)?TAG:strTag);
        requestQueue.add(request);
    }

    public void cancelPendingRequest(Object objTag){
        if(requestQueue!= null){
            requestQueue.cancelAll(objTag);
        }
    }
}
