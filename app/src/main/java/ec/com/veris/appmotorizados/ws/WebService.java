package ec.com.veris.appmotorizados.ws;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Map;

import ec.com.veris.appmotorizados.util.Url;

/**
 * Created by luis on 18/09/2016.
 */
public class WebService<T> {
    public static final String TAG = WebService.class.getSimpleName();
    private Context context;

    public WebService(Context context){
        this.context = context;
    }

    public void consumirWsGET(String url, Map<String, String> params, final VolleyCallback callback) {
        String URL = Url.setParameters(url, params);

        AppSingleton.getInstance(context).addToRequestQueue(
            new GsonRequest<T>(Request.Method.GET,
                               URL,
                               null,
                               null,
                               createMyReqSuccessListener(callback),
                               createMyReqErrorListener(callback)
                               ));
    }

    public void consumirWsPOST(String url, Map<String, String> params, final VolleyCallback callback) {
        AppSingleton.getInstance(context).addToRequestQueue(
            new GsonRequest<T>(Request.Method.POST,
                               url,
                               null,
                               params,
                               createMyReqSuccessListener(callback),
                               createMyReqErrorListener(callback)
                               ));
    }

    private Response.Listener<T> createMyReqSuccessListener(final VolleyCallback callback){
        return new Response.Listener<T>(){
            public void onResponse(T response){
                callback.onSuccessResponse((String)response);
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener(final VolleyCallback callback){
        return new Response.ErrorListener(){
            public void onErrorResponse(VolleyError e){
                callback.onErrorResponse("Error al consumir Servicio Web : " + e.toString());
            }
        };
    }

    /*TODO := Invocacion GsonRequest
      public void consumirWsGET(String url, Map<String, String> params, final VolleyCallback callback) {
        String URL = Url.setParameters(url, params);

        AppSingleton.getInstance(context).addToRequestQueue(
                new GsonRequest<Resultado>(URL,
                        Request.Method.GET,
                        Resultado.class,
                        null,
                        null,
                        new Response.Listener<Resultado>(){
                            public void onResponse(Resultado response){
                                callback.onSuccessResponse(response);
                            }
                        },
                        new Response.ErrorListener(){
                            public void onErrorResponse(VolleyError e){
                                callback.onErrorResponse("Error al consumir Servicio Web : " + e.toString());
                            }
                        }
                ));

    }*/

}
