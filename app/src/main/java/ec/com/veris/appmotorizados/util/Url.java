package ec.com.veris.appmotorizados.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by luis on 17/09/2016.
 */
public class Url {

    public static String setParameters(String url, Map<String, String> m){
        String URL = null;
        if(url!= null){

            String key;
            String value;

            StringBuilder stringBuilder = new StringBuilder(url);
            if(m!= null && m.size() > 0){
                int i = 0;
                for (Map.Entry<String, String> entry: m.entrySet()) {
                    try {
                        key   = URLEncoder.encode(entry.getKey(),"UTF-8");
                        value = URLEncoder.encode(entry.getValue(),"UTF-8");

                        if(i == 0){
                            stringBuilder.append("?" + key + "=" + value);
                        }else{
                            stringBuilder.append("&" + key + "=" + value);
                        }

                    } catch (UnsupportedEncodingException e){
                       e.printStackTrace();
                    }
                    i++;
                }
            }
            URL = stringBuilder.toString();
        }
        return URL;
    }
}
