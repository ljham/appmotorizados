package ec.com.veris.appmotorizados.ws;

/**
 * Created by luis on 19/09/2016.
 */
public interface VolleyCallback {

    void onSuccessResponse(String resultado);

    void onErrorResponse(String error);
}
