package ec.com.veris.appmotorizados.ws;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by luis.jama on 13/09/2016.
 */
public class GsonRequest<T> extends Request<T> {

    private final Gson gson = new Gson();
    private final Map<String, String> headers;
    private final Map<String, String> params;
    private final Listener<T> listener;

    public GsonRequest(int metodo,
                       String url,
                       Map<String, String> headers,
                       Map<String, String> params,
                       Listener<T> listener,
                       ErrorListener errorListener
                      ){
        super(metodo, url, errorListener);
        this.headers  = headers;
        this.params   = params;
        this.listener = listener;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            String json = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers,"UTF-8"));
            T bean = (T)json;
            return Response.success(bean, HttpHeaderParser.parseCacheHeaders(networkResponse));
        }catch (UnsupportedEncodingException e){
            return Response.error(new ParseError());
        } catch (JsonSyntaxException e){
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public Map<String, String> getHeaders()throws AuthFailureError{
        return headers!= null ? headers : super.getHeaders();
    }

    @Override
    protected Map<String,String> getParams()throws AuthFailureError{
        return params;
    }

}
