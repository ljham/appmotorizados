package ec.com.veris.appmotorizados.beans;

import java.util.List;

/**
 * Created by luis.jama on 13/09/2016.
 */
public class Solicitudes {

    private Integer numeroSolicitud;
    private String estado;
    private String origen;
    private String nombreSolicitante;
    private String numeroTelefono;
    private String tipoDireccion;
    private String direccionPrincipal;
    private String direccionReferencial;
    private String observacionesCall;
    private String observacionesFlebo;
    private String latitud;
    private String longitud;
    private String codigoTipoIdentFactura;
    private String nombreTipoIdentFactura;
    private String nombreFactura;
    private String correoElectronicoFactura;
    private String imeiDispositivo;
    private Agendamiento agendamiento;
    private List<OrdenesXSolicitud> lsOrdenesXSolicitud;
    private List<FormasPagoXSolicitud> lsFormasPagoXSolicitud;

    public Integer getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(Integer numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getTipoDireccion() {
        return tipoDireccion;
    }

    public void setTipoDireccion(String tipoDireccion) {
        this.tipoDireccion = tipoDireccion;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getDireccionReferencial() {
        return direccionReferencial;
    }

    public void setDireccionReferencial(String direccionReferencial) {
        this.direccionReferencial = direccionReferencial;
    }

    public String getObservacionesCall() {
        return observacionesCall;
    }

    public void setObservacionesCall(String observacionesCall) {
        this.observacionesCall = observacionesCall;
    }

    public String getObservacionesFlebo() {
        return observacionesFlebo;
    }

    public void setObservacionesFlebo(String observacionesFlebo) {
        this.observacionesFlebo = observacionesFlebo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCodigoTipoIdentFactura() {
        return codigoTipoIdentFactura;
    }

    public void setCodigoTipoIdentFactura(String codigoTipoIdentFactura) {
        this.codigoTipoIdentFactura = codigoTipoIdentFactura;
    }

    public String getNombreTipoIdentFactura() {
        return nombreTipoIdentFactura;
    }

    public void setNombreTipoIdentFactura(String nombreTipoIdentFactura) {
        this.nombreTipoIdentFactura = nombreTipoIdentFactura;
    }

    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    public String getCorreoElectronicoFactura() {
        return correoElectronicoFactura;
    }

    public void setCorreoElectronicoFactura(String correoElectronicoFactura) {
        this.correoElectronicoFactura = correoElectronicoFactura;
    }

    public String getImeiDispositivo() {
        return imeiDispositivo;
    }

    public void setImeiDispositivo(String imeiDispositivo) {
        this.imeiDispositivo = imeiDispositivo;
    }

    public Agendamiento getAgendamiento() {
        return agendamiento;
    }

    public void setAgendamiento(Agendamiento agendamiento) {
        this.agendamiento = agendamiento;
    }

    public List<OrdenesXSolicitud> getLsOrdenesXSolicitud() {
        return lsOrdenesXSolicitud;
    }

    public void setLsOrdenesXSolicitud(List<OrdenesXSolicitud> lsOrdenesXSolicitud) {
        this.lsOrdenesXSolicitud = lsOrdenesXSolicitud;
    }

    public List<FormasPagoXSolicitud> getLsFormasPagoXSolicitud() {
        return lsFormasPagoXSolicitud;
    }

    public void setLsFormasPagoXSolicitud(List<FormasPagoXSolicitud> lsFormasPagoXSolicitud) {
        this.lsFormasPagoXSolicitud = lsFormasPagoXSolicitud;
    }
}
