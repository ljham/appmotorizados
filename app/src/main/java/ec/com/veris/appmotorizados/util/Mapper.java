package ec.com.veris.appmotorizados.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ec.com.veris.appmotorizados.beans.Agendamiento;
import ec.com.veris.appmotorizados.beans.Resultado;

/**
 * Created by luis on 05/11/2016.
 */
public class Mapper<T> {
    private Gson gson = new Gson();

    public <T> T jsonToObject(String json, Class outputClass){
        T outputObject = null;
        try {
            if(json!= null){
                if (outputObject == null) {
                    outputObject = (T) outputClass.newInstance();
                }
                outputObject = (T) gson.fromJson(json, outputClass);
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return outputObject;
    }

    public <T> T jsonToObject(String json, TypeToken typeToken){
        T outputObject = null;
        try {
            if(json!= null && typeToken!= null){
                outputObject = (T) gson.fromJson(json, typeToken.getType());
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return outputObject;
    }

}
