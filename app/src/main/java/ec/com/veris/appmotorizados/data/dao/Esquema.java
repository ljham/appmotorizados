package ec.com.veris.appmotorizados.data.dao;

/**
 * Created by luis on 27/11/2016.
 */
public class Esquema {

    public Esquema(){}

    interface ColumnasParametros{
        String AMBIENTE = "ambiente";
    }

    interface ColumnasSolicitudes{
        String NUMERO_SOLICITUD = "numero_solicitud";
        String ESTADO = "estado";
        String ORIGEN = "origen";
        String NOMBRE_SOLICITANTE = "nombre_solicitante";
        String NUMERO_TELEFONO = "numero_telefono";
        String TIPO_DIRECCION = "tipo_direccion";
        String DIRECCION_PRINCIPAL = "direccion_principal";
        String DIRECCION_REFERENCIAL = "direccion_referencial";
        String OBSERVACIONES_CALL = "observaciones_call";
        String OBSERVACIONES_FLEBO = "observaciones_flebo";
        String LATITUD = "latitud";
        String LONGITUD = "longitud";
        String CODIGO_TIPO_IDENT_FACTURA = "codigo_tipo_ident_factura";
        String NOMBRE_TIPO_IDENT_FACTURA = "nombre_tipo_ident_factura";
        String NOMBRE_FACTURA = "nombre_factura";
        String CORREO_ELECTRONICO_FACTURA = "correo_electronico_factura";
        String IMEI_DISPOSITIVO = "imei_dispositivo";
        String ES_ACTIVO = "es_activo";

    }

    interface ColumnasAgendamiento{
        String SECUENCIA_AGENDAMIENTO = "secuencia_agendamiento";
        String NUMERO_SOLICITUD = "numero_solicitud";
        String CODIGO_PROFESIONAL = "codigo_profesional";
        String FECHA_ATENCION = "fecha_atencion";
        String HORA_ATENCION = "hora_atencion";
        String USUARIO_AGENDAMIENTO = "usuario_agendamiento";
        String FECHA_AGENDAMIENTO = "fecha_agendamiento";
        String ES_ACTIVO = "es_activo";

    }

    interface ColumnasOrdenesXSolicitud{
        String NUMERO_SOLICITUD = "numero_solicitud";
        String SECUENCIAL = "secuencial";
        String CODIGO_EMPRESA = "codigo_empresa";
        String NUMERO_ORDEN = "numero_orden";
        String TIPO_FACTURACION = "tipo_facturacion";
        String CODIGO_TIPO_IDENTIFICACION = "codigo_tipo_identificacion";
        String NOMBRE_TIPO_IDENTIFICACION = "nombre_tipo_identificacion";
        String NUMERO_IDENTIFICACION = "numero_identificacion";
        String NOMBRE_PACIENTE = "nombre_paciente";
        String FECHA_NACIMIENTO = "fecha_nacimiento";
        String EDAD = "edad";
        String GENERO = "genero";
        String ES_DISCAPACITADO = "es_discapacitado";
        String MAIL = "mail";
        String TELEFONO_FIJO = "telefono_fijo";
        String TELEFONO_MOVIL = "telefono_movil";
        String NOMBRE_CONVENIO = "nombre_convenio";
        String NOMBRE_PAQUETE = "nombre_paquete";
        String SECUENCIA_COMPROBANTE = "secuencia_comprobante";
        String ES_ACTIVO = "es_activo";
    }

    interface ColumnasDetalleOrdenServicio{
        String CODIGO_EMPRESA = "codigo_empresa";
        String NUMERO_ORDEN = "numero_orden";
        String LINEA_DETALLE = "linea_detalle";
        String CODIGO_PRESTACION = "codigo_prestacion";
        String NOMBRE_PRESTACION = "nombre_prestacion";
        String CANTIDAD = "cantidad";
        String SUBTOTAL_VENTA = "subtotal_venta";
        String SUBTOTAL_CLIENTE = "subtotal_cliente";
        String SUBTOTAL_COPAGO = "subtotal_copago";
        String VALOR_IVA = "valor_iva";
        String VALOR_TOTAL = "valor_total";
        String CODIGO_ESTADO = "codigo_estado";
        String ES_ACTIVO = "es_activo";
    }

    interface ColumnasFormasPagoXSolicitud{
        String NUMERO_SOLICITUD = "numero_solicitud";
        String SECUENCIA = "secuencia";
        String CODIGO_FORMA_PAGO = "codigo_forma_pago";
        String NOMBRE_FORMA_PAGO = "nombre_forma_pago";
        String VALOR = "valor";
        String CODIGO_INSTITUCION = "codigo_institucion";
        String NOMBRE_INSTITUCION = "nombre_institucion";
        String NUMERO_CHEQUE = "numero_cheque";
        String NUMERO_CUENTA = "numero_cuenta";
        String NOMBRE_CUENTA = "nombre_cuenta";
        String ES_ACTIVO = "es_activo";
    }

    public static class Solicitudes implements ColumnasSolicitudes{}
    public static class Agendamiento implements ColumnasAgendamiento{}
    public static class OrdenesXSolicitud implements ColumnasOrdenesXSolicitud{}
    public static class DetalleOrdenServicio implements ColumnasDetalleOrdenServicio{}
    public static class FormasPagoXSolicitud implements ColumnasFormasPagoXSolicitud{}


}
