package ec.com.veris.appmotorizados.beans;

/**
 * Created by luis on 05/11/2016.
 */
public class DetalleOrdenServicio {

    private Integer codigoEmpresa;
    private Integer numeroOrden;
    private Integer lineaDetalle;
    private Integer codigoPrestacion;
    private String nombrePrestacion;
    private Integer cantidad;
    private Double subtotalVenta;
    private Double subtotalCliente;
    private Double subtotalCopago;
    private Double valorIva;
    private Double valorTotal;
    private String estado;

    public Integer getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(Integer codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public Integer getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(Integer numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public Integer getLineaDetalle() {
        return lineaDetalle;
    }

    public void setLineaDetalle(Integer lineaDetalle) {
        this.lineaDetalle = lineaDetalle;
    }

    public Integer getCodigoPrestacion() {
        return codigoPrestacion;
    }

    public void setCodigoPrestacion(Integer codigoPrestacion) {
        this.codigoPrestacion = codigoPrestacion;
    }

    public String getNombrePrestacion() {
        return nombrePrestacion;
    }

    public void setNombrePrestacion(String nombrePrestacion) {
        this.nombrePrestacion = nombrePrestacion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getSubtotalVenta() {
        return subtotalVenta;
    }

    public void setSubtotalVenta(Double subtotalVenta) {
        this.subtotalVenta = subtotalVenta;
    }

    public Double getSubtotalCliente() {
        return subtotalCliente;
    }

    public void setSubtotalCliente(Double subtotalCliente) {
        this.subtotalCliente = subtotalCliente;
    }

    public Double getSubtotalCopago() {
        return subtotalCopago;
    }

    public void setSubtotalCopago(Double subtotalCopago) {
        this.subtotalCopago = subtotalCopago;
    }

    public Double getValorIva() {
        return valorIva;
    }

    public void setValorIva(Double valorIva) {
        this.valorIva = valorIva;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
